import React from "react";

// Styles
import "./page-container.layout.scss";

interface Props {
  children: React.ReactNode;
  title?: string;
}

const PageContainer = (props: Props) => {
  const { children, title } = props;

  return (
    <div className="pageContainer-container">
      <div className="pageContainer-top">
        <h1>{title? title: "Titulo"}</h1>
      </div>
      <div>{children}</div>
    </div>
  );
};

export default PageContainer;
