import React, { useState } from "react";

// Styles
import "./sidebar.component.scss";

// React router
import { withRouter } from "react-router-dom";

// Devextreme
import { Tooltip } from "devextreme-react/tooltip";

class SidebarItem {
  id: string;
  redirection: string;
  icon: string;
  description: string;

  constructor(
    id: string,
    redirection: string,
    icon: string,
    description: string
  ) {
    this.id = id;
    this.redirection = redirection;
    this.icon = icon;
    this.description = description;
  }
}

const SidebarComponent = (props: any) => {
  const { history } = props;

  const defaultSidebarItems: SidebarItem[] = [
    new SidebarItem("EjemplosUI", "/", "fas fa-circle-notch", "Ejemplos de ui"),
    new SidebarItem(
      "Recursos",
      "/resources",
      "fab fa-buromobelexperte",
      "Manejo de recursos"
    ),
    new SidebarItem(
      "Votacion",
      "/elections",
      "fas fa-vote-yea",
      "Crear votación"
    ),
    new SidebarItem("Votos", "/vote", "fas fa-box-tissue", "Votar"),
    new SidebarItem(
      "Resultados",
      "/results",
      "fas fa-chart-pie",
      "Ver resultados"
    ),
  ];

  const [sidebarItems] = useState(defaultSidebarItems);
  const [hoveredItem, setHoveredItem] = useState<SidebarItem | null>(null);
  const [hoveredItemIndex, setHoveredItemIndex] = useState<number | null>(null);

  return (
    <div className="sidebar-container">
      {/* Tooltip de devextreme */}
      {hoveredItem ? (
        <Tooltip
          target={`#${hoveredItem.id}${hoveredItemIndex}`}
          visible={true}
          closeOnOutsideClick={false}
          position="right"
        >
          <div>{hoveredItem?.description}</div>
        </Tooltip>
      ) : null}

      {/* Item de sidebar */}
      {sidebarItems.map((item, i) => {
        return (
          <div
            id={item.id + i}
            key={i}
            className="sidbar-item"
            onClick={() => history.push(item.redirection)}
            onMouseOver={() => {
              setHoveredItem(item);
              setHoveredItemIndex(i);
            }}
            onMouseLeave={() => {
              setHoveredItem(null);
              setHoveredItemIndex(null);
            }}
          >
            <i className={item.icon}></i>
          </div>
        );
      })}
    </div>
  );
};

export default withRouter(SidebarComponent);
