// React
import React, { useEffect, Fragment } from "react";

// DevExtreme
import notify from "devextreme/ui/notify";

// Shared
import { ToastRxjs } from "../../rxjs";
import { Toast } from "../../models";
import { ToastTypeEnum } from "../../enums";

const ToastComponent: React.FC<{}> = ({ children }) => {
  // Subscribirse a eventos de toast
  useEffect(() => {
    ToastRxjs.ToastObserver.subscribe((toast) => handleToastEvent(toast));
  }, []);

  /**
   * Encargado de mostrar mensaje de toast
   * @param toast
   */
  const handleToastEvent = (toast: Toast) => {
    if (!toast.show) return;
    switch (toast.type) {
      case ToastTypeEnum.error:
        return notify(toast.message, toast.type, 600);
      case ToastTypeEnum.info:
        return notify(toast.message, toast.type, 600);
      case ToastTypeEnum.success:
        return notify(toast.message, toast.type, 600);
      case ToastTypeEnum.warning:
        return notify(toast.message, toast.type, 600);
      default:
        return;
    }
  };

  return <Fragment>{children}</Fragment>;
};

export default ToastComponent;
