// React
import React, { useEffect, useState, Fragment } from "react";

// Styles
import "./page-loader.component.scss";

// Shared
import { PageLoaderRxjs } from "../../rxjs";
import { PageLoader } from "../../models";

const PageLoaderComponent: React.FC<{}> = ({ children }) => {
  const [showPageLoaderState, setShowPageLoaderState] = useState(false);

  useEffect(() => {
    // Subscribirse a cambios de page loader
    PageLoaderRxjs.PageLoaderObserver.subscribe((pageLoader) =>
      handlePageLoader(pageLoader)
    );
  }, []);

  /**
   * Maneja mostrar u ocultar page loader
   * @param pageLoader
   */
  const handlePageLoader = (pageLoader: PageLoader) => {
    if (pageLoader.show) {
      setShowPageLoaderState(true);
    } else {
      setShowPageLoaderState(false);
    }
  };

  return (
    <Fragment>
      {children}
      {showPageLoaderState ? (
        // En caso de mostrar page loader
        <div className="pageLoader-container">
          <div className="loader">Loading...</div>
        </div>
      ) : // En caso de ocultar page loader
      null}
    </Fragment>
  );
};

export default PageLoaderComponent;
