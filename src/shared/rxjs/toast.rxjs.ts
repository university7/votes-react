import { BehaviorSubject } from "rxjs";
import { Toast } from "../models/toast.model";
import { ToastTypeEnum } from "../enums/toastType.enum";

const initialToast = new Toast(false, ToastTypeEnum.error, "");

const ToastObserver = new BehaviorSubject(initialToast);

const showErrorToast = (message: string) => {
  const toast = new Toast(true, ToastTypeEnum.error, message);
  ToastObserver.next(toast);
};

const showInfoToast = (message: string) => {
  const toast = new Toast(true, ToastTypeEnum.info, message);
  ToastObserver.next(toast);
};

const showSuccessToast = (message: string) => {
  const toast = new Toast(true, ToastTypeEnum.success, message);
  ToastObserver.next(toast);
};

const showWarningToast = (message: string) => {
  const toast = new Toast(true, ToastTypeEnum.warning, message);
  ToastObserver.next(toast);
};

export const ToastRxjs = {
  ToastObserver,
  showErrorToast,
  showInfoToast,
  showSuccessToast,
  showWarningToast
}