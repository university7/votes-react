import { BehaviorSubject } from "rxjs";
import { PageLoader } from "../models";

const initialPageLoader = new PageLoader(false, "");

const PageLoaderObserver = new BehaviorSubject(initialPageLoader);

const showPageLoader = (message?: string) => {
  PageLoaderObserver.next({ show: true, message });
};

const hidePageLoader = () => {
  PageLoaderObserver.next({ show: false });
};

export const PageLoaderRxjs = {
  PageLoaderObserver,
  showPageLoader,
  hidePageLoader,
};
