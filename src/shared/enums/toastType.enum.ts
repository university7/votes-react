export enum ToastTypeEnum {
    error = "error",
    info = "info",
    success = "success",
    warning = "warning"
}