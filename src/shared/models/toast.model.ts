import { ToastTypeEnum } from "../enums/toastType.enum";

export class Toast {
  show: boolean;
  type: ToastTypeEnum;
  message: string;

  constructor(show: boolean, type: ToastTypeEnum, message: string) {
    this.show = show;
    this.type = type;
    this.message = message;
  }
}
