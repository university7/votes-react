export class PageLoader {
  show: boolean;
  message?: string;

  constructor(show: boolean, message: string | any) {
    this.show = show;
    this.message = message;
  }
}
