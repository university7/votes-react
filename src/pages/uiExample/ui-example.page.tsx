import React, { useEffect, useState, Fragment } from "react";

// DevExtreme
import Button from "devextreme-react/button";
import DataGrid, { Column, Pager, Paging } from "devextreme-react/data-grid";
import Form, { SimpleItem, GroupItem, Label } from "devextreme-react/form";

// Shared
import { ToastRxjs, PageLoaderRxjs } from "../../shared/rxjs";

class ExampleModel {
  id: string;
  name: string;
  description: string;
  photo: string;

  constructor(id: string, name: string, description: string, photo: string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.photo = photo;
  }
}

const UiExample = () => {
  const [exampleListState, setExampleListState] = useState<ExampleModel[]>([]);

  /**
   * Encargado de armar lista de objetos de tipo ejemplo aleatorios
   */
  useEffect(() => {
    PageLoaderRxjs.showPageLoader();
    const numberObjects = 17;
    let exampleList: any = [];
    for (let i = 0; i <= numberObjects; i++) {
      const id = Math.random().toString();
      const name = `Nombre #${i}`;
      const description = `Nombre #${i}`;
      const randomPhoto = "https://picsum.photos/id/237/200/300";

      exampleList.push(new ExampleModel(id, name, description, randomPhoto));
    }
    setExampleListState(exampleList);
    PageLoaderRxjs.hidePageLoader();
  }, []);

  const getDivSeparator = () => {
    return (
      <Fragment>
        {" "}
        <br></br>
        <hr></hr>
        <br></br>
      </Fragment>
    );
  };

  return (
    <div>
      <h3>Botones de toast</h3>
      <div>
        <Button
          text="error"
          onClick={() => ToastRxjs.showErrorToast("error")}
        />
        <Button text="info" onClick={() => ToastRxjs.showInfoToast("info")} />
        <Button
          text="success"
          onClick={() => ToastRxjs.showSuccessToast("success")}
        />
        <Button
          text="warning"
          onClick={() => ToastRxjs.showWarningToast("warning")}
        />
      </div>
      {getDivSeparator()}
      <h3>Mostrar spinner</h3>
      <div>
        <Button
          text="Mostrar spinner"
          onClick={() => {
            PageLoaderRxjs.showPageLoader();
            setTimeout(() => {
              PageLoaderRxjs.hidePageLoader();
            }, 2500);
          }}
        />
      </div>
      {getDivSeparator()}
      {/* https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/ */}
      <h3>Grid</h3>
      <DataGrid
        dataSource={exampleListState}
        export={{ enabled: true }}
        filterRow={{ visible: true }}
        groupPanel={{ allowColumnDragging: true }}
        searchPanel={{ visible: true }}
        onRowDblClick={(e) => alert("Item doble clic")}
        columnHidingEnabled={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        hoverStateEnabled={true}
        showColumnLines={true}
        showRowLines={true}
        showBorders={true}
        columnAutoWidth={false}
        onSelectionChanged={() => alert("Selección cambió")}
      >
        <Paging defaultPageSize={10} />
        <Pager
          showPageSizeSelector={true}
          allowedPageSizes={[5, 10, 20]}
          showInfo={true}
        />

        <Column dataField="id" caption="Id" />
        <Column dataField="name" caption="Nombre" />
        <Column dataField="description" caption="Descripción" />
        <Column dataField="photo" caption="Imágen" />

        <Column type="buttons" width={100}>
          <Button
            text="Eliminar"
            onClick={() => alert("Eliminar item")}
          ></Button>
        </Column>
      </DataGrid>
      {getDivSeparator()}
      {/* https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxForm/ */}
      <h3>Formulario</h3>
      <Form formData={exampleListState[0]} labelLocation="top">
        <GroupItem colCount={3}>
          <SimpleItem dataField="id">
            <Label text="Id" />
          </SimpleItem>
          <SimpleItem dataField="name">
            <Label text="Nombre" />
          </SimpleItem>
          <SimpleItem dataField="description">
            <Label text="Descripción" />
          </SimpleItem>
          <SimpleItem dataField="photo">
            <Label text="Imágen" />
          </SimpleItem>
        </GroupItem>
      </Form>
      <div>
        <p>Botones de guardado y cancelar por hacer</p>
      </div>
      {getDivSeparator()}
      {/* https://js.devexpress.com/Demos/WidgetsGallery/Demo/Charts/PieWithResolvedLabelOverlapping/React/Light/ */}
      <h3>Gráficas</h3>
    </div>
  );
};

export default UiExample;
