// React
import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

// React Router
import { BrowserRouter } from "react-router-dom";

// Redux
import { Provider } from "react-redux";
import Store from "./store/store";
import ToastComponent from "./shared/ui/toast/toast.component";
import PageLoaderComponent from "./shared/ui/page-loader/page-loader.component";

ReactDOM.render(
  <BrowserRouter>
    <Provider store={Store}>
      <React.StrictMode>
        <PageLoaderComponent>
          <ToastComponent>
            <App />
          </ToastComponent>
        </PageLoaderComponent>
      </React.StrictMode>
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
