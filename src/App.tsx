// React
import React, { useEffect } from "react";
import "./App.scss";

// DevExtreme
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.css";

// React router
import { Switch, Route, withRouter } from "react-router-dom";

// Pages
import UiExample from "./pages/uiExample/ui-example.page";
import Page401 from "./pages/page401/page401.page";
import Resources from "./pages/resources/resources.page";
import Vote from "./pages/vote/vote.page";
// import Login from "./pages/login/login.page";
import PageContainer from "./layout/page-container/page-container.layout";
import SidebarComponent from "./layout/sidebar/sidebar.component";
import Results from "./pages/results/results.page";
import Election from "./pages/elections/election.page";
import { PageLoaderRxjs } from "./shared/rxjs";

const App = () => {
  useEffect(() => {
    PageLoaderRxjs.showPageLoader();
    setTimeout(() => {
      PageLoaderRxjs.hidePageLoader();
    }, 2000);
  }, []);
  return (
    <div className="app-container">
      <SidebarComponent />
      <Switch>
        <Route
          path="/resources"
          render={() => (
            <PageContainer title="Asignar recursos">
              <Resources />
            </PageContainer>
          )}
        />
        <Route
          path="/vote"
          render={() => (
            <PageContainer title="Votar">
              <Vote />
            </PageContainer>
          )}
        />
        <Route
          path="/results"
          render={() => (
            <PageContainer title="Ver resultados">
              <Results />
            </PageContainer>
          )}
        />
        <Route
          path="/elections"
          render={() => (
            <PageContainer title="Crear votación">
              <Election />
            </PageContainer>
          )}
        />
        {/* <Route path="/login" component={Login} /> */}
        <Route
          path="/"
          exact
          render={() => (
            <PageContainer title="Ejemplos uso de ui">
              <UiExample />
            </PageContainer>
          )}
        />
        <Route component={Page401} />
      </Switch>
    </div>
  );
};

export default withRouter(App);
