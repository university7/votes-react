import * as types from "../types/session.types";

const initialState = {
  user: null,
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.SET_USER:
      return { ...state, user: action.payload };
    case types.DELETE_USER:
      return { ...state, user: null };
    default:
      return state;
  }
};

export default reducer;
