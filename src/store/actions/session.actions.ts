import * as types from "../types/session.types";

export const setUser = (user: any) => (dispatch: any) => {
  dispatch({
    type: types.SET_USER,
    user: user,
  });
};

export const deleteUser = () => (dispatch: any) => {
    dispatch({
        type: types.DELETE_USER
    })
}